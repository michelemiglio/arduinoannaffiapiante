# ArduinoAnnaffiaPiante

*Autori:* Michele Miglio

**Descrizione**

Un problema tipico d’estate con molte piante è quello di dare la la giusta quantità di acqua per far si che la terra resti umida nel vaso (non fradicia e non secca), situazione ideale per in generale per molte piante (con ovviamente specificità diverse tra le varie specie) nello specifico ideale per i bonsai che coltivo.
Attraverso un Arduino Uno e 3 sensori di umidità FC-28, vado a monitorare, in loop, 3 punti del vaso misurando lo stato di umidità mediandola poi a 2 a 2 così da avere una corretta stima dell'umidità nella sezione del terreno.
Studiato il corretto range di umidità terra asciutta-terra bagnata (e anche asciuto-bagnato) è possibile ricavare un dato utile per comprendere lo stato del terreno.
Qualora il valore sia fuori da un certo range editabile, il sistema va ad attivare/aumentare (o spegnere/diminuire) delle pompe che portino l'acqua nel vaso apportando in autonomia le correzioni allo stato del vaso.
In questo progetto le pompe sono simulate con dei semplici led la cui potenza aumenta o diminuisce in base all'aumento o alla diminuzione del flusso di acqua.


**Hardware**

*  Arduino Uno: [https://store.arduino.cc/arduino-uno-rev3](https://store.arduino.cc/arduino-uno-rev3)
*  Sensore umidità FC-28: [https://www.robotstore.it/Sensore-di-Umidità-del-suolo](https://www.robotstore.it/Sensore-di-Umidità-del-suolo)
*  Led
*  Cavi
*  Breadboard


**Codice**

Per comprendere l'utilizzo del sensore ho seguito questa guida: [https://www.9minuti.it/controllare-lumidita-del-terreno-con-arduino/#.Xg--4-LPyHo](https://www.9minuti.it/controllare-lumidita-del-terreno-con-arduino/#.Xg--4-LPyHo)

Nel repository sono presenti due file equivalenti, con una sola differenza:
*  codice tarato su asciutto(aria) e bagnato(acqua): [https://gitlab.com/michelemiglio/arduinoannaffiapiante/blob/master/misura_umidita_no_vaso.ino](https://gitlab.com/michelemiglio/arduinoannaffiapiante/blob/master/misura_umidita_no_vaso.ino)
*  codice tarato su asciuto(terra) e bagnato(terra): [https://gitlab.com/michelemiglio/arduinoannaffiapiante/blob/master/misura_umidita_vaso.ino](https://gitlab.com/michelemiglio/arduinoannaffiapiante/blob/master/misura_umidita_vaso.ino)


**Immagini**

[Foto 1](https://gitlab.com/michelemiglio/arduinoannaffiapiante/blob/master/photo5868495459461476998.jpg)
[Foto 2](https://gitlab.com/michelemiglio/arduinoannaffiapiante/blob/master/photo5868495459461476997.jpg)
[Foto 3](https://gitlab.com/michelemiglio/arduinoannaffiapiante/blob/master/photo5868495459461476999.jpg)
[Foto 4](https://gitlab.com/michelemiglio/arduinoannaffiapiante/blob/master/photo5868495459461477000.jpg)