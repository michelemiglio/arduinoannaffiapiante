//ingresso dati sensore di umidità
#define pinSensorDestra A0
#define pinSensorCentro A1
#define pinSensorSinistra A2

//pin della "pompa" (il led)
#define pinPompaSinistra 10
#define pinPompaDestra 11

//valore sensore
int sensorValueDestra;
int sensorValueCentro;
int sensorValueSinistra;

int mediaUmidoSinistra;
int mediaUmidoDestra;

//potenza pompa = dati led
int potenzaSinistra = 0 ;
int potenzaDestra = 0 ;

int regolazione = 5;

void setup() {

  //inizializzo il pin della pompa
  pinMode(pinPompaSinistra, OUTPUT);
  pinMode(pinPompaDestra, OUTPUT);
  Serial.begin(9600);
}
 
void loop() {

  if(mediaUmidoSinistra<30 && potenzaSinistra>0){
    //SE < 20 significa che l'umidità è bassa quindi il vaso è asciutto in media meno del 20% quindi devo fermare l'acqua
    //vario la potenza per il prossimo loop
    potenzaSinistra = potenzaSinistra - regolazione;
    
    Serial.println("Diminuisci Acqua");
    Serial.println();
    }

    
  if(mediaUmidoDestra<30 && potenzaDestra>0){
    //SE < 20 significa che l'umidità è bassa quindi il vaso è asciutto in media meno del 20% quindi devo fermare l'acqua
    //vario la potenza per il prossimo loop
    potenzaDestra = potenzaDestra - regolazione;
    
    Serial.println("Diminuisci Acqua");
    Serial.println();
    }
    
 //misurazione valore umidità
  sensorValueDestra = analogRead(pinSensorDestra);
  sensorValueCentro = analogRead(pinSensorCentro);
  sensorValueSinistra = analogRead(pinSensorSinistra);

  //regolazione misura: da prove sperimentali l'immersione in acqua porta valore 375 (la porto a 0 circa) e asciutto 1022 (la porto a 100)
  //NB il valore approssimata varia da caso a caso
  
  sensorValueDestra = map(sensorValueDestra, 375, 1000, 0, 100);
  sensorValueCentro = map(sensorValueCentro, 375, 1000, 0, 100);
  sensorValueSinistra = map(sensorValueSinistra, 375, 980, 0, 100);

/*
  Serial.print("Valore sensorValueDestra: ");
  Serial.println(sensorValueDestra);
  Serial.print("Valore sensorValueCentro: ");
  Serial.println(sensorValueCentro);
  Serial.print("Valore sensorValueSinistra: ");
  Serial.println(sensorValueSinistra);
*/

  //medio l'umidità dei sensori per capire in media quanto la terra del vaso è bagnata in media
  mediaUmidoDestra=(sensorValueDestra+sensorValueDestra+sensorValueCentro)/3;
  mediaUmidoSinistra=(sensorValueSinistra+sensorValueSinistra+sensorValueCentro)/3;
  //NB valore ALTO corrisponde ad ASCIUTTO mentre valore BASSO a BAGNATO


  Serial.print("Valore Medio Sinistra: ");
  Serial.println(mediaUmidoSinistra);
  Serial.print("Valore Medio Destra: ");
  Serial.println(mediaUmidoDestra);
  
  if(mediaUmidoSinistra > 60 && potenzaSinistra < 255){
    //SE > 60 significa che l'umidità è bassa quindi il vaso è asciutto in media più del 60% quindi devo accendere l'acqua
    //vario la potenza per il prossimo loop
    potenzaSinistra = potenzaSinistra + regolazione;
    Serial.println("Aumenta Acqua");
    Serial.println();
    }

    if(mediaUmidoDestra > 60 && potenzaDestra < 255){
    //SE > 60 significa che l'umidità è bassa quindi il vaso è asciutto in media più del 60% quindi devo accendere l'acqua
    //vario la potenza per il prossimo loop
    potenzaDestra = potenzaDestra + regolazione;
    Serial.println("Aumenta Acqua");
    Serial.println();
    }


    Serial.print("potenzaSinistra: ");
    Serial.println(potenzaSinistra);
    Serial.print("potenzaDestra: ");
    Serial.println(potenzaDestra);
    Serial.println();

    //variazione potenza "pompa Sinistra"
    analogWrite(pinPompaSinistra, potenzaSinistra);
    //variazione potenza "pompa Destra"
    analogWrite(pinPompaDestra, potenzaDestra);

    delay(3000);

}
